var express = require("express");
var web3 = require("web3");
var Tx = require("ethereumjs-tx").Transaction;
var router = express.Router();
var abiArray = require("./abi");

/* GET home page. */
router.get("/", function(req, res, next) {
  res.render("index", { title: "Express" });
});

router.get("/sendTx", function(req, res) {
  //Infura HttpProvider Endpoint
  web3js = new web3(
    new web3.providers.HttpProvider(
      "ENTER_INFURA_PROVIDER"
    )
  );

  var myAddress = "0x1aD9F16F7E694864E45463dCF8f1810e3E57Ec80";
  var privateKey = Buffer.from(
    "ENTER_YOUR_PRIVATE_KEY",
    "hex"
  );
  var toAddress = "0x51970972c27D779070B780171f5E4caE77DEE76B";

  var count;
  // get transaction count, later will used as nonce
  web3js.eth.getTransactionCount(myAddress).then(function(v) {
    console.log("Count: " + v);
    count = v;
    var temp = web3js.utils.toWei("0.5");
    var amount = web3js.utils.toHex(temp);
    //creating raw tranaction
    var rawTransaction = {
      from: myAddress,
      gasPrice: web3js.utils.toHex(20 * 1e9),
      gasLimit: web3js.utils.toHex(21000),
      to: toAddress,
      value: amount,
      nonce: web3js.utils.toHex(count)
    };
    console.log(rawTransaction);
    //creating tranaction via ethereumjs-tx
    var transaction = new Tx(rawTransaction, {
      chain: "ropsten",
      hardfork: "petersburg"
    });
    //signing transaction with private key
    transaction.sign(privateKey);
    //sending transacton via web3js module
    web3js.eth
      .sendSignedTransaction("0x" + transaction.serialize().toString("hex"))
      .on("transactionHash", console.log);

  });
});

module.exports = router;
